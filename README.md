
##HuaweiTest

The code with the solution is provided in the repository in the file main.cpp.

The result of compilation should be console application.

#Algorithm
Now let's look at the algorithm. First, we transform the graph by decreasing each weight by the minimum weight. Now let's have a tank with width W. To check whether it will pass, it is enough to remove all edges with weights less than W and check the resulting graph for connectivity. But it is obvious that if a tank of width X passes, then any tank with a smaller width can also pass. Therefore, we can use binsearch by answer and check if two vertices are reachable. But it is clear that we do not need to iterate over any width, it is enough to cut off the edges that we will remove in the sorted array of weights.
n - number of vertices
m - number of edges
Final algorithm:
1. transfrom the graph by decreasing each weight by the minimum weight(O(n + m))
2. sort edges by weight(mlog(m))
3. binsearch and check for connectivity(log(m) iterations of binsearch and O(n + m) for checking for connectivity on each iteration, so O((n + m)log(m))

Final asymptotics:
O((n + m)log(m))

Memory:
O(n^2 + m) - because of adjacency matrix.


