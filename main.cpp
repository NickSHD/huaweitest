#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <climits>
#include <unordered_set>
#include <unordered_map>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include <set>

struct Edge {

    typedef unsigned long long ull;
    typedef unsigned int ui;

    ui first;
    ui second;
    ull weight = 0;

};

bool operator<(const Edge& left, const Edge& right) {
    return left.weight < right.weight;
}


class Graph {
public:

    typedef unsigned long long ull;
    typedef unsigned int ui;

    Graph(std::vector<Edge>& edge, ui s, ui t, ui n) : s_(s - 1), t_(t - 1), number_of_vertices_(n) {

        adj_matrix_.resize(n);
        for (size_t i = 0; i < n; i++) {
            adj_matrix_[i].resize(n);
        }

        std::sort(edge.begin(), edge.end());
        ull min_weight = edge[0].weight;

        for (size_t i = 0; i < edge.size(); i++) {
            ull temp_w = edge[i].weight - min_weight;
            adj_matrix_[edge[i].first - 1][edge[i].second - 1] = true;
            adj_matrix_[edge[i].second - 1][edge[i].first - 1] = true;
            if (weights_map_.find(temp_w) == weights_map_.end()) {
                weights_.push_back(temp_w);
            }
            weights_map_[temp_w].push_back(edge[i]);
        }
    }

    double max_width() {
        ui left = 0;
        ui right = number_of_vertices_;

        while (right - left > 1) {
            ui mid = (right + left) / 2;
            if (is_connected(mid)) {
                left = mid;
            } else {
                right = mid;
            }
        }
        return weights_[left];
    }



private:

    bool is_connected(ui m) {

        for (size_t k = 0; k < m; k++) {
            for (size_t i = 0; i < weights_map_[weights_[k]].size(); i++) {
                ui first = weights_map_[weights_[k]][i].first;
                ui second = weights_map_[weights_[k]][i].second;
                adj_matrix_[first - 1][second - 1] = false;
                adj_matrix_[second - 1][first - 1] = false;
            }
        }

        std::vector<bool> col(number_of_vertices_, false);
        dfs(s_, col);

        for (size_t k = 0; k < m; k++) {
            for (size_t i = 0; i < weights_map_[weights_[k]].size(); i++) {
                ui first = weights_map_[weights_[k]][i].first;
                ui second = weights_map_[weights_[k]][i].second;
                adj_matrix_[first - 1][second - 1] = true;
                adj_matrix_[second - 1][first - 1] = true;
            }
        }
        return col[t_];
    }

    void dfs(ui v, std::vector<bool>& col) {
        col[v] = true;
        for (size_t i = 0; i < number_of_vertices_; i++) {
            if (adj_matrix_[v][i] == 1 && !col[i]) {
                dfs(i, col);
            }
        }
    }


    ui s_, t_, number_of_vertices_;
    std::vector<std::vector<bool>> adj_matrix_;
    std::vector<ull> weights_;
    std::unordered_map<ull, std::vector<Edge>> weights_map_;

};

class GenerateGraph {
public:

    typedef unsigned long long ull;
    typedef unsigned int ui;

    const ull W = 5000;

    GenerateGraph(ui n, ui m) : n(n), m(m) {
        std::vector<ui> prufer_code(n - 2);
        for (size_t i = 0; i < n - 2; i++) {
            prufer_code[i] = rand() % n;
        }

        std::vector<Edge> tree_edges = prufer_decode(prufer_code);
        edges.resize(m);
        for (size_t i = 0; i < tree_edges.size(); i++) {
            Edge cur = edges[i];
            edges[i] = {cur.first + 1, cur.second + 1, generate_weight()};
        }

        for (size_t i = tree_edges.size(); i < m; i++) {
            edges[i].weight = generate_weight();
            do {
                edges[i].first = rand() % n + 1;
                edges[i].second = rand() % n + 1;
            } while (edges[i].first == edges[i].second);
        }
        do {
            s = rand() % n + 1;
            t = rand() % n + 1;
        } while (s == t);
    }

    ull generate_weight() {
        return ((static_cast<int64_t>(rand()) << 48) ^ (static_cast<int64_t>(rand()) << 32)
               ^ (rand() << 16) ^ rand()) % W + 1;
    }

    std::vector<Edge> prufer_decode(const std::vector<ui>& prufer_code) {
        std::vector<ui> degree (n, 1);
        for (size_t i = 0; i < n - 2; i++) {
            degree[prufer_code[i]]++;
        }

        std::set<ui> leaves;
        for (size_t i = 0; i < n; i++) {
            if (degree[i] == 1) {
                leaves.insert(i);
            }
        }
        std::vector<Edge> tree_edges;
        for (size_t i = 0; i < n - 2; ++i) {
            ui leaf = *leaves.begin();
            leaves.erase(leaves.begin());

            ui v = prufer_code[i];
            tree_edges.push_back ({leaf, v});
            if (--degree[v] == 1)
                leaves.insert (v);
        }
        tree_edges.push_back({*leaves.begin(), *--leaves.end()});
        return tree_edges;

    }

    ui n, m, s, t;
    std::vector<Edge> edges;
};


void test_function(const std::string& file_name) {

    std::ifstream fin(file_name.c_str());

    unsigned int n, m, s, t;
    fin >> n >> m >> s >> t;
    GenerateGraph generate(n, m);
    std::vector<Edge> edges(m);
    for (size_t i = 0; i < m; i++) {
        fin >> edges[i].first >> edges[i].second >> edges[i].weight;
    }
    fin.close();

    Graph graph(edges, s, t, n);

    std::cout << "Armata's width: (0, " << graph.max_width() << "]" << '\n';
}



int main(int argc, char *argv[]) {
    std::string type(argv[1]);
    if (type == "-g") {
        unsigned int n, m, s, t;

        n = atoi(argv[2]);
        m = atoi(argv[3]);

        GenerateGraph generate(n, m);
        s = generate.s;
        t = generate.t;

        std::string file_name(argv[4]);
        std::ofstream fout(file_name.c_str());
        fout << n << " " << m << " " << s << " " << t << '\n';

        for (size_t i = 0; i < generate.edges.size(); i++) {
            fout << generate.edges[i].first << " " <<
                generate.edges[i].second << " " << generate.edges[i].weight << '\n';
        }
        fout.close();
        return 0;

    }
    if (type == "-t") {
        test_function(argv[2]);
        return 0;
    }


}
